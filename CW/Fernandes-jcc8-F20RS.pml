mtype = { normal, critical, openned, closed, enabld, disabled, reset };

mtype pressure = critical;
mtype escape_valve = closed;
mtype alarm = disabled;
int value = 10;

chan sensor_ctr = [1] of {mtype};
chan alarm_ctr = [1] of {mtype};
chan console_ctr = [1] of {mtype};

#define eo (escape_valve == openned)
#define ae (alarm == enabled)
#define pn (pressure == normal)
#define pc (pressure == critical)

// LTL that pressure is either normal or critical
ltl p1 { [](pn || pc) }


// LTL if pressure is critical the escape_valve will be openned
ltl p2 { []( pc -> <> eo ) }


// LTL if the alarm is enabled then the escape_valve will be openned
ltl p3 { []( ae -> <> eo ) }


proctype env(chan s, c, a)
{
    do

    :: ( escape_valve == openned ) -> value = value - 1;
    :: ( escape_valve == closed ) -> value = value + 1;
    :: ( value <= 0) -> value = 0;
    :: ( value >= 20) -> value = 20;

    :: (s?[critical]) ->
        s?critical;
        pressure = critical;
        alarm = enabld;

    :: (s?[normal]) ->
        s?normal;
        pressure = normal;

    :: (c?[reset]) ->
        c?reset;
        alarm = disabled;
        escape_valve = closed;

    :: (a?[enabld]) ->
        a?enabld;
        escape_valve = openned;

    od
}

proctype control(chan s, c, a)
{
    do
        :: ( value >= 10 ) -> s!critical;

        :: ( value < 10 ) -> s!normal;

        :: ( (pressure == normal) && (alarm == enabld) && (escape_valve == openned) ) -> c!reset;

        :: ( (alarm == enabld) && (escape_valve == closed) ) -> a!enabld;
    od
}

proctype monitor()
{
    do
    :: assert( (pressure == normal) || (pressure == critical) );
    od
}

init
{
    atomic
    {
        run control(sensor_ctr, console_ctr, alarm_ctr);
        run env(sensor_ctr, console_ctr, alarm_ctr);
        run monitor();
    }
}
