mtype = { done }


chan hello_to_control = [0] of {mtype};
chan control_to_world = [0] of {mtype};


proctype hello(chan c){
    printf("Hello");
    c!done;
}

proctype world(chan c){
    c?done;
    printf("world");
}

proctype control(chan h, w)
{
    h?done;
    w!done;
}


init
{
    atomic
    {
        run world(control_to_world);
        run hello(hello_to_control);
        run control(hello_to_control, control_to_world);
    }
}
