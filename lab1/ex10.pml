mtype = {open, close};

mtype in = open, out = close;
byte water_level = 25, user_reserve = 0;


proctype user()
{
    do
    :: (user_reserve > 0) -> user_reserve - 1;
    :: true -> skip;
    od
}

proctype inlet()
{
    do
    :: (in == open) -> water_level = water_level + 1;
    od
}

proctype outlet()
{
    do
    :: (out == open) ->
            water_level = water_level - 1;
            user_reserve = user_reserve + 1;
    od
}

proctype sensors()
{
    do
    :: atomic
        {
             (water_level <= 20) ->
                    in = open;
                    out = close;
        }
    :: atomic
        {
            (water_level >= 30) ->
                    in = close;
                    out = open;
        }
    od
}

proctype monitor()
{
    assert( (water_level >= 20) && (water_level <= 30) )
}

init
{
    atomic
    {
        run user();
        run inlet();
        run outlet();
        run sensors();
    }
}
