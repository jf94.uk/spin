mtype = { done }

chan coin_insert = [0] of { mtype };
chan consume_choc = [0] of { mtype };

proctype vender(chan coin)
{

}

proctype chocoholic(chan consume)
{

}



init
{
    atomic
    {
        run chocoholic()
    }
}
