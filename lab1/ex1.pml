proctype hello(){
    printf("Hello")
}

proctype world(){
    printf(" World")
}

init
{
    atomic
    {
        run hello();
        run world();
    }
}
