mtype = { coin20, coin50, milk, plain };

chan pay = [1] of { mtype };
chan eat = [1] of { mtype};



proctype vender(chan p, e)
{
    do
    :: p?coin20 -> e!milk;
    :: p?coin50 -> e!plain;
    od
}


proctype customer(chan p, e)
{
    do
    :: p!coin20 -> e?milk;
    :: p!coin50 -> e?plain;
    od
}


init
{
    atomic
    {
        run vender(pay, eat);
        run customer(pay, eat);
    }
}

