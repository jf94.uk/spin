mtype = { milk, plain };

chan pay = [1] of { byte };
chan eat = [1] of { mtype};

int box = 0, budget = 450;

proctype vender(chan pc, e)
{
    assert(box + budget == 450)
    do
    :: ( (budget >= 20) && pc?[20] ) ->
        pc?20;
        e!milk;
        box = box + 20;
        budget = budget - 20;
        assert(box + budget == 450)
    :: ( (budget >= 50) && pc?[50] ) ->
        pc?50;
        e!plain;
        box = box + 50;
        budget = budget - 50;
        assert(box + budget == 450)
    od
}


proctype customer(chan pc, e)
{
    do
    :: pc!20 -> e?milk;
    :: pc!50 -> e?plain;
    od
}


init
{
    atomic
    {
        run vender(pay, eat);
        run customer(pay, eat);
    }
}
