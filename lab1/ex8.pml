mtype = { milk, plain };

chan pay = [1] of { byte };
chan eat = [1] of { mtype};

byte m = 10, p = 5, box = 0;

proctype vender(chan pc, e)
{
    assert(box == (450 - ( (m*20) + (p*50) ) ) );
    do
    :: ( (m > 0) && pc?[20] ) ->
        pc?20;
        e!milk;
        box = box + 20;
        m = m - 1;
        assert(box == (450 - ( (m*20) + (p*50) ) ) );
    :: ( (p > 0) && pc?[50] ) ->
        pc?50;
        e!plain;
        box = box + 50;
        p = p - 1;
        assert(box == (450 - ( (m*20) + (p*50) ) ) );
    od
}


proctype customer(chan pc, e)
{
    do
    :: pc!20 -> e?milk;
    :: pc!50 -> e?plain;
    od
}


init
{
    atomic
    {
        run vender(pay, eat);
        run customer(pay, eat);
    }
}
