byte value1 = 1, value2 = 2, value3 = 3;

active proctype A() {
    value3 = value3 + value2;
}
active proctype B() {
    value2 = value2 + value1;
}

active proctype monitor()
{
    do
    :: assert( value3 == 5 || value3 == 6 || value3 == 3);
    od
}


init
{
    atomic
    {
        run monitor();
        run A();
        run B();
    }

}

